module.exports = function s4(x) {
    if(x==undefined || typeof(x) != 'object' || x.length == 0) return [];
    let a =Object.values(x)
    let s = a.join(' ')
    let n = s.toLowerCase()
    n= n.split(' ')
    for (let i=0; i<n.length;i++) {
        if (/[^a-z]/.test(n[i])) return 'name should only contains letters'
        else n[i] = n[i].charAt(0).toUpperCase() + n[i].substring(1,n[i].length)
    }
    return n.join(' ')
}
