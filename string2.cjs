module.exports = function s2(x) {
    let z = x.replace(/[^.0-9]/g,'#')
    if (z.includes('#') || x.split('.').length!= 4) return []
    for (let i of x.split('.')) {
    	if(Number(i)>255) return []
    }
    let a =[]
    for (let j of x.split('.')) {
        a.push(Number(j))
    }
    return a
}

